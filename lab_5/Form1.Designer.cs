﻿
namespace lab_5
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea5 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series5 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.richTextBox2 = new System.Windows.Forms.RichTextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.IsBalanced = new System.Windows.Forms.RadioButton();
            this.IsNatural = new System.Windows.Forms.RadioButton();
            this.speed = new System.Windows.Forms.TextBox();
            this.numbers = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.SuspendLayout();
            // 
            // richTextBox2
            // 
            this.richTextBox2.Location = new System.Drawing.Point(12, 161);
            this.richTextBox2.Name = "richTextBox2";
            this.richTextBox2.Size = new System.Drawing.Size(424, 277);
            this.richTextBox2.TabIndex = 16;
            this.richTextBox2.Text = "";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(12, 105);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 15;
            this.button2.Text = "Тест";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // IsBalanced
            // 
            this.IsBalanced.AutoSize = true;
            this.IsBalanced.Location = new System.Drawing.Point(26, 82);
            this.IsBalanced.Name = "IsBalanced";
            this.IsBalanced.Size = new System.Drawing.Size(232, 17);
            this.IsBalanced.TabIndex = 14;
            this.IsBalanced.TabStop = true;
            this.IsBalanced.Text = "Сбалансированное трехпутевое слияние";
            this.IsBalanced.UseVisualStyleBackColor = true;
            // 
            // IsNatural
            // 
            this.IsNatural.AutoSize = true;
            this.IsNatural.Checked = true;
            this.IsNatural.Location = new System.Drawing.Point(26, 59);
            this.IsNatural.Name = "IsNatural";
            this.IsNatural.Size = new System.Drawing.Size(215, 17);
            this.IsNatural.TabIndex = 13;
            this.IsNatural.TabStop = true;
            this.IsNatural.Text = "Cортировка естественным слиянием";
            this.IsNatural.UseVisualStyleBackColor = true;
            // 
            // speed
            // 
            this.speed.Location = new System.Drawing.Point(164, 29);
            this.speed.Name = "speed";
            this.speed.Size = new System.Drawing.Size(65, 20);
            this.speed.TabIndex = 12;
            this.speed.Text = "0,5";
            // 
            // numbers
            // 
            this.numbers.Location = new System.Drawing.Point(93, 29);
            this.numbers.Name = "numbers";
            this.numbers.Size = new System.Drawing.Size(65, 20);
            this.numbers.TabIndex = 11;
            this.numbers.Text = "10";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 27);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 10;
            this.button1.Text = "Анимация";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(246, 29);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(65, 20);
            this.richTextBox1.TabIndex = 9;
            this.richTextBox1.Text = "";
            // 
            // chart1
            // 
            chartArea5.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea5);
            this.chart1.Location = new System.Drawing.Point(459, 29);
            this.chart1.Name = "chart1";
            series5.ChartArea = "ChartArea1";
            series5.Name = "Series1";
            this.chart1.Series.Add(series5);
            this.chart1.Size = new System.Drawing.Size(454, 409);
            this.chart1.TabIndex = 17;
            this.chart1.Text = "chart1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(925, 539);
            this.Controls.Add(this.chart1);
            this.Controls.Add(this.richTextBox2);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.IsBalanced);
            this.Controls.Add(this.IsNatural);
            this.Controls.Add(this.speed);
            this.Controls.Add(this.numbers);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.richTextBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox richTextBox2;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.RadioButton IsBalanced;
        private System.Windows.Forms.RadioButton IsNatural;
        private System.Windows.Forms.TextBox speed;
        private System.Windows.Forms.TextBox numbers;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
    }
}

