﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using Excel = Microsoft.Office.Interop.Excel;

namespace lab_5
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();    
            
        }

        void CreateFile(string FileName, int[] array)
        {
            BinaryWriter FileFull = new BinaryWriter(File.Open(FileName, FileMode.Create));

            foreach (int i in array)
                FileFull.Write( Convert.ToString(i) );

            FileFull.Close();

        } //создание файла и заполнение данными из массива
         
        void AppendFile(string FileName, string value)
        {
            using (BinaryWriter file = new BinaryWriter(File.Open(FileName, FileMode.Append)))
                file.Write(value);

        } //Дозапись в файл

        string LastValueString(string FileName)
        {
            string value = "";
            using (BinaryReader file = new BinaryReader(File.Open(FileName, FileMode.Open)))
            {
                while (file.PeekChar() > -1)
                    value = file.ReadString();
            }

            return value;
        }//Последние строковое значение

        bool ExistStopSymbol(string FileName, string stop)
        {

            using(BinaryReader file = new BinaryReader(File.Open(FileName, FileMode.Open)))
            {
                while(file.PeekChar() > -1)
                    if (file.ReadString() == stop && file.PeekChar() > -1)
                    return true;
            }

            return false;
        
        }//существование стоп символа(игнорирует стоп символ в конце файла)

        string FileToStr(string FileName)
        {
            string str = "";
            using (BinaryReader file = new BinaryReader(File.Open(FileName, FileMode.Open)))
                while (file.PeekChar() > -1) str += file.ReadString() + " ";

            return str;
        }

        void NaturalFusion(string FileName)
        {
            string MergeFile_name = "merge";
            string Part1_name = "part1";
            string Part2_name = "part2";
            string part = "part";
            do
            {

                //инициализация 2 подфайлов
                string last_numbr = "";

                using (BinaryReader OrigFile = new BinaryReader(File.Open(FileName, FileMode.Open)))
                {

                    //запись первого значения в первый подфайл
                    string temp = OrigFile.ReadString();
                    using (BinaryWriter Part1 = new BinaryWriter(File.Open(Part1_name, FileMode.Create)))
                        Part1.Write(temp);

                    using (BinaryWriter Part2 = new BinaryWriter(File.Open(Part2_name, FileMode.Create)));

                    int last_write_file = 0;
                    //0 - первый файл
                    //1 - второй файл


                    while (OrigFile.PeekChar() > -1)
                    {
                        temp = OrigFile.ReadString();

                        last_numbr = LastValueString(part + (last_write_file + 1));

                        if (last_numbr != "`")
                        {

                            if (Convert.ToInt32(last_numbr) <= Convert.ToInt32(temp))

                                AppendFile(part + (last_write_file + 1), temp);
                            else
                            {
                                AppendFile(part + (last_write_file + 1), "~");
                                last_write_file = ((last_write_file + 1) % 2);
                                AppendFile(part + (last_write_file + 1), temp);
                            }
                        }
                        else
                            AppendFile(part + (last_write_file + 1), temp);


                    }

                    if (LastValueString(Part1_name) != "~")
                        AppendFile(Part1_name, "~");

                    if (LastValueString(Part2_name) != "~")
                        AppendFile(Part2_name, "~");

                }

                //слияние
                using (BinaryWriter OrigFile = new BinaryWriter(File.Open(FileName, FileMode.Create)))
                {

                    BinaryReader part1 = new BinaryReader(File.Open(Part1_name, FileMode.Open));
                    BinaryReader part2 = new BinaryReader(File.Open(Part2_name, FileMode.Open));

                    string value_1 = part1.ReadString();
                    string value_2 = part2.ReadString();

                    while (part1.PeekChar() > -1 || part2.PeekChar() > -1)
                    {

                        switch (value_1)
                        {
                            case "~":

                                while (value_2 != "~" && part2.PeekChar() > -1)
                                {
                                    OrigFile.Write(value_2);
                                    value_2 = part2.ReadString();
                                }

                                if (part1.PeekChar() > -1)
                                    value_1 = part1.ReadString();

                                if (part2.PeekChar() > -1)
                                    value_2 = part2.ReadString();

                                break;
                            default:

                                switch (value_2)
                                {
                                    case "~":

                                        while (value_1 != "~" && part1.PeekChar() > -1)
                                        {
                                            OrigFile.Write(value_1);
                                            value_1 = part1.ReadString();
                                        }

                                        if (part1.PeekChar() > -1)
                                            value_1 = part1.ReadString();

                                        if (part2.PeekChar() > -1)
                                            value_2 = part2.ReadString();

                                        break;

                                    default:

                                        if (Convert.ToInt32(value_1) < Convert.ToInt32(value_2))
                                        {
                                            OrigFile.Write(value_1);
                                            value_1 = part1.ReadString();
                                        }

                                        else
                                        {
                                            OrigFile.Write(value_2);
                                            value_2 = part2.ReadString();
                                        }

                                        break;
                                }
                                break;

                        }
                    }

                    part1.Dispose();
                    part2.Dispose();

                }

                //bool f1 = ExistStopSymbol(Part1_name, "~");
                //bool f2 = ExistStopSymbol(Part2_name, "~");

                //string text1 = "";
                //using (BinaryReader file = new BinaryReader(File.Open(Part1_name, FileMode.Open)))
                //    while (file.PeekChar() > -1)
                //        text1 += file.ReadString() + " ";

                //string text2 = "";
                //using (BinaryReader file = new BinaryReader(File.Open(Part2_name, FileMode.Open)))
                //    while (file.PeekChar() > -1)
                //        text2 += file.ReadString() + " ";

                //string merge = "";
                //using (BinaryReader file = new BinaryReader(File.Open(FileName, FileMode.Open)))
                //    while (file.PeekChar() > -1)
                //        merge += file.ReadString() + " ";


            } while (ExistStopSymbol(Part1_name, "~") || ExistStopSymbol(Part2_name, "~"));
        }//сортировка естественным слиянием

        void BalancedMerge(string FileName)
        {
            int M = 3;
            int T = 1;
            //M - кол-во вспомогательных файлов
            //T - кол-во файлов для сияний

            string part_name = "part_";
            string merge_name = "merge";

            //создание вспомогательные файлов для слияний
            //using (BinaryWriter MergeN = new BinaryWriter(File.Open(merge_name, FileMode.Create)));


            string value;
            int last_write_file = 0;
            bool flag_write = false;
            
            do
            {

                string main_str = FileToStr(FileName);

                using (BinaryReader main_file = new BinaryReader(File.Open(FileName, FileMode.Open)))
                {
                    for (int i = 1; i <= M; i++) //создание вспомогательные файлов
                        using (BinaryWriter PartN = new BinaryWriter(File.Open(part_name + i, FileMode.Create)));

                    
                    while (main_file.PeekChar() > -1)
                    {
                        value = main_file.ReadString();

                        

                        for (int i = 1; i <= M; i++)
                            if (Convert.ToInt32(value) >= Convert.ToInt32(LastValueString(part_name + i) == "" ? "0" : LastValueString(part_name + i)))
                            {
                                AppendFile(part_name + i, value); last_write_file = i - 1; flag_write = true;
                                break;
                            }
                        //else if(i == M)
                        //        AppendFile(part_name + (last_write_file + 1 % M), value);

                        if (flag_write == false)
                        {
                            last_write_file = (last_write_file + 1) % M;
                            AppendFile(part_name + (last_write_file + 1), value);
                        }

                        flag_write = false;
                    }

                    

                } //запись значий в вспом.файлы из основного

                AppendFile(part_name + 1, Convert.ToString(Int32.MaxValue)); AppendFile(part_name + 2, Convert.ToString(Int32.MaxValue)); AppendFile(part_name + 3, Convert.ToString(Int32.MaxValue));

                string s1 = FileToStr(part_name + 1), s2 = FileToStr(part_name + 2), s3 = FileToStr(part_name + 3);
                string merge = FileToStr(FileName);

                using (BinaryWriter Merge = new BinaryWriter(File.Open(FileName, FileMode.Create)))
                {
                    BinaryReader part1 = new BinaryReader(File.Open(part_name + 1, FileMode.Open));
                    BinaryReader part2 = new BinaryReader(File.Open(part_name + 2, FileMode.Open));
                    BinaryReader part3 = new BinaryReader(File.Open(part_name + 3, FileMode.Open));

                    int value_1 = Convert.ToInt32(part1.ReadString());
                    int value_2 = Convert.ToInt32(part2.ReadString());
                    int value_3 = Convert.ToInt32(part3.ReadString());
                    int min;

                    do
                    {
                        min = Math.Min(value_1, Math.Min(value_2, value_3));
                        Merge.Write( Convert.ToString(Math.Min(value_1, Math.Min(value_2, value_3))) );

                        if (Math.Min(value_1, Math.Min(value_2, value_3)) == value_1 && part1.PeekChar() > -1)
                        {
                            if (part1.PeekChar() > -1) value_1 = Convert.ToInt32(part1.ReadString());
                        }
                        else if (Math.Min(value_1, Math.Min(value_2, value_3)) == value_2 && part2.PeekChar() > -1)
                        {
                            if (part2.PeekChar() > -1) value_2 = Convert.ToInt32(part2.ReadString());
                        }
                        else if (Math.Min(value_1, Math.Min(value_2, value_3)) == value_3 && part3.PeekChar() > -1)
                        {
                            if (part3.PeekChar() > -1) value_3 = Convert.ToInt32(part3.ReadString());
                        }

                        if (part1.PeekChar() == -1) value_1 = Int32.MaxValue;
                        if (part2.PeekChar() == -1) value_2 = Int32.MaxValue;
                        if (part3.PeekChar() == -1) value_3 = Int32.MaxValue;

                    } while (part1.PeekChar() > -1 || part2.PeekChar() > -1 || part3.PeekChar() > -1);

                    part1.Dispose(); part2.Dispose(); part3.Dispose();
                }

                merge = FileToStr(FileName);
                s1 = FileToStr(part_name + 1); s2 = FileToStr(part_name + 2); s3 = FileToStr(part_name + 3);

            } while (FileToStr(part_name + 2) != "2147483647 " && FileToStr(part_name + 3) != "2147483647 ");




            

        }//Сбалансированное трехпутевое слияние

        void NaturalFusionAnim(string FileName, List<int> anim, ref int count)
        {
            string MergeFile_name = "merge";
            string Part1_name = "part1";
            string Part2_name = "part2";
            string part = "part";
            string[] str_temp;

            do
            {

                //инициализация 2 подфайлов
                string last_numbr = "";

                using (BinaryReader OrigFile = new BinaryReader(File.Open(FileName, FileMode.Open)))
                {

                    //запись первого значения в первый подфайл
                    string temp = OrigFile.ReadString();
                    using (BinaryWriter Part1 = new BinaryWriter(File.Open(Part1_name, FileMode.Create)))
                        Part1.Write(temp);

                    using (BinaryWriter Part2 = new BinaryWriter(File.Open(Part2_name, FileMode.Create))) ;

                    int last_write_file = 0;
                    //0 - первый файл
                    //1 - второй файл


                    while (OrigFile.PeekChar() > -1)
                    {
                        temp = OrigFile.ReadString();

                        last_numbr = LastValueString(part + (last_write_file + 1));

                        if (last_numbr != "`")
                        {

                            if (Convert.ToInt32(last_numbr) <= Convert.ToInt32(temp))

                                AppendFile(part + (last_write_file + 1), temp);
                            else
                            {
                                AppendFile(part + (last_write_file + 1), "~");
                                last_write_file = ((last_write_file + 1) % 2);
                                AppendFile(part + (last_write_file + 1), temp);
                            }
                        }
                        else
                            AppendFile(part + (last_write_file + 1), temp);


                    }

                    if (LastValueString(Part1_name) != "~")
                        AppendFile(Part1_name, "~");

                    if (LastValueString(Part2_name) != "~")
                        AppendFile(Part2_name, "~");

                }

                //слияние
                using (BinaryWriter OrigFile = new BinaryWriter(File.Open(FileName, FileMode.Create)))
                {

                    BinaryReader part1 = new BinaryReader(File.Open(Part1_name, FileMode.Open));
                    BinaryReader part2 = new BinaryReader(File.Open(Part2_name, FileMode.Open));

                    string value_1 = part1.ReadString();
                    string value_2 = part2.ReadString();

                    while (part1.PeekChar() > -1 || part2.PeekChar() > -1)
                    {

                        switch (value_1)
                        {
                            case "~":

                                while (value_2 != "~" && part2.PeekChar() > -1)
                                {
                                    OrigFile.Write(value_2);
                                    value_2 = part2.ReadString();
                                }

                                if (part1.PeekChar() > -1)
                                    value_1 = part1.ReadString();

                                if (part2.PeekChar() > -1)
                                    value_2 = part2.ReadString();

                                break;
                            default:

                                switch (value_2)
                                {
                                    case "~":

                                        while (value_1 != "~" && part1.PeekChar() > -1)
                                        {
                                            OrigFile.Write(value_1);
                                            value_1 = part1.ReadString();
                                        }

                                        if (part1.PeekChar() > -1)
                                            value_1 = part1.ReadString();

                                        if (part2.PeekChar() > -1)
                                            value_2 = part2.ReadString();

                                        break;

                                    default:

                                        if (Convert.ToInt32(value_1) < Convert.ToInt32(value_2))
                                        {
                                            OrigFile.Write(value_1);
                                            value_1 = part1.ReadString();
                                        }

                                        else
                                        {
                                            OrigFile.Write(value_2);
                                            value_2 = part2.ReadString();
                                        }

                                        break;
                                }
                                break;

                        }
                    }

                    part1.Dispose();
                    part2.Dispose();

                }

                str_temp = FileToStr(FileName).Split(' ');
                foreach (string str in str_temp)
                {
                    if(str != "" && str != "~") anim.Add( Convert.ToInt32(str)  );
                }
                
                count += 1;

            } while (ExistStopSymbol(Part1_name, "~") || ExistStopSymbol(Part2_name, "~"));
        }

        void BalancedMergeAnim(string FileName, List<int> anim, ref int count)
        {
            int M = 3;
            int T = 1;
            //M - кол-во вспомогательных файлов
            //T - кол-во файлов для сияний

            string part_name = "part_";
            string merge_name = "merge";

            //создание вспомогательные файлов для слияний
            //using (BinaryWriter MergeN = new BinaryWriter(File.Open(merge_name, FileMode.Create)));


            string value;
            int last_write_file = 0;
            bool flag_write = false;
            string[] str_temp;

            do
            {

                string main_str = FileToStr(FileName);

                using (BinaryReader main_file = new BinaryReader(File.Open(FileName, FileMode.Open)))
                {
                    for (int i = 1; i <= M; i++) //создание вспомогательные файлов
                        using (BinaryWriter PartN = new BinaryWriter(File.Open(part_name + i, FileMode.Create))) ;


                    while (main_file.PeekChar() > -1)
                    {
                        value = main_file.ReadString();



                        for (int i = 1; i <= M; i++)
                            if (Convert.ToInt32(value) >= Convert.ToInt32(LastValueString(part_name + i) == "" ? "0" : LastValueString(part_name + i)))
                            {
                                AppendFile(part_name + i, value); last_write_file = i - 1; flag_write = true;
                                break;
                            }
                        //else if(i == M)
                        //        AppendFile(part_name + (last_write_file + 1 % M), value);

                        if (flag_write == false)
                        {
                            last_write_file = (last_write_file + 1) % M;
                            AppendFile(part_name + (last_write_file + 1), value);
                        }

                        flag_write = false;
                    }



                } //запись значий в вспом.файлы из основного

                AppendFile(part_name + 1, Convert.ToString(Int32.MaxValue)); AppendFile(part_name + 2, Convert.ToString(Int32.MaxValue)); AppendFile(part_name + 3, Convert.ToString(Int32.MaxValue));

                string s1 = FileToStr(part_name + 1), s2 = FileToStr(part_name + 2), s3 = FileToStr(part_name + 3);
                string merge = FileToStr(FileName);

                using (BinaryWriter Merge = new BinaryWriter(File.Open(FileName, FileMode.Create)))
                {
                    BinaryReader part1 = new BinaryReader(File.Open(part_name + 1, FileMode.Open));
                    BinaryReader part2 = new BinaryReader(File.Open(part_name + 2, FileMode.Open));
                    BinaryReader part3 = new BinaryReader(File.Open(part_name + 3, FileMode.Open));

                    int value_1 = Convert.ToInt32(part1.ReadString());
                    int value_2 = Convert.ToInt32(part2.ReadString());
                    int value_3 = Convert.ToInt32(part3.ReadString());
                    int min;

                    do
                    {
                        min = Math.Min(value_1, Math.Min(value_2, value_3));
                        Merge.Write(Convert.ToString(Math.Min(value_1, Math.Min(value_2, value_3))));

                        if (Math.Min(value_1, Math.Min(value_2, value_3)) == value_1 && part1.PeekChar() > -1)
                        {
                            if (part1.PeekChar() > -1) value_1 = Convert.ToInt32(part1.ReadString());
                        }
                        else if (Math.Min(value_1, Math.Min(value_2, value_3)) == value_2 && part2.PeekChar() > -1)
                        {
                            if (part2.PeekChar() > -1) value_2 = Convert.ToInt32(part2.ReadString());
                        }
                        else if (Math.Min(value_1, Math.Min(value_2, value_3)) == value_3 && part3.PeekChar() > -1)
                        {
                            if (part3.PeekChar() > -1) value_3 = Convert.ToInt32(part3.ReadString());
                        }

                        if (part1.PeekChar() == -1) value_1 = Int32.MaxValue;
                        if (part2.PeekChar() == -1) value_2 = Int32.MaxValue;
                        if (part3.PeekChar() == -1) value_3 = Int32.MaxValue;

                    } while (part1.PeekChar() > -1 || part2.PeekChar() > -1 || part3.PeekChar() > -1);

                    part1.Dispose(); part2.Dispose(); part3.Dispose();
                }

                merge = FileToStr(FileName);
                s1 = FileToStr(part_name + 1); s2 = FileToStr(part_name + 2); s3 = FileToStr(part_name + 3);

                str_temp = FileToStr(FileName).Split(' ');
                foreach (string str in str_temp)
                {
                    if (str != "" && str != "2147483647") anim.Add(Convert.ToInt32(str));
                }

                count += 1;

            } while (FileToStr(part_name + 2) != "2147483647 " && FileToStr(part_name + 3) != "2147483647 ");

        }

        static void init_array(ref int[] array, int size)
        {
            Random rnd = new Random();
            int number;
            array = new int[size];
            List<int> list_numbers = new List<int>();

            for (int i = 0; i < size; i++)
            {
                while (true)
                {
                    number = rnd.Next(1, size + 1);
                    if (list_numbers.IndexOf(number) == -1) { list_numbers.Add(number); break; }
                }

                array[i] = number;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int size = Convert.ToInt32(numbers.Text);
            double time = Convert.ToDouble(speed.Text);
            int[] array = new int[size];
            init_array(ref array, size);

            CreateFile("file", array);


            //BalancedMerge("file");

            //richTextBox1.Text += FileToStr("file") + "\n";
            //richTextBox1.Text += "test";

            Stopwatch stopWatch;
            chart1.Series[0].Points.Clear();
            richTextBox1.Text = String.Empty;
            List<int> anim = new List<int>(); int count = 1;

            if (IsNatural.Checked)
            {
                stopWatch = Stopwatch.StartNew();
                NaturalFusionAnim("file", anim, ref count);
                stopWatch.Stop();

                animation(anim, size, count, time);
                richTextBox1.Text = stopWatch.ElapsedTicks.ToString();
                stopWatch.Reset();
            }
            else if (IsBalanced.Checked)
            {
                stopWatch = Stopwatch.StartNew();
                BalancedMergeAnim("file", anim, ref count);
                stopWatch.Stop();

                animation(anim, size, count, time);
                richTextBox1.Text = stopWatch.Elapsed.Ticks.ToString();
                stopWatch.Reset();

            }


        }

        async void animation(List<int> anim, int size, int count, double time)
        {
            button1.Enabled = false;

            chart1.ChartAreas[0].Axes[0].Maximum = size;
            chart1.ChartAreas[0].Axes[1].Maximum = size;
            chart1.Series[0].Points.Clear();

            for (int i = 0; i < count; i++) //разы
            {
                for (int j = i * size; j < j + size && j < anim.Count; j++) //вывод соот. элементов
                    chart1.Series[0].Points.Add(anim[j]);
                await Task.Delay(TimeSpan.FromSeconds(time));
                if (i < count - 2) chart1.Series[0].Points.Clear();
            }

            button1.Enabled = true;

        }

        string CreateReport(List<long> method1, List<long> method2, int count, int sizes, ref int[] sizes_array, string[] methodsName)
        {


            Excel.Application excel_app = new Excel.Application();

            //отобразить exel 
            excel_app.Visible = false;
            //Количество листов в рабочей книге
            excel_app.SheetsInNewWorkbook = 1;
            //Добавить рабочую книгу
            Excel.Workbook workBook = excel_app.Workbooks.Add(Type.Missing);
            //Отключить отображение окон с сообщениями
            excel_app.DisplayAlerts = false;
            //Получаем первый лист документа (счет начинается с 1)
            Excel.Worksheet sheet = (Excel.Worksheet)excel_app.Worksheets.get_Item(1);

            long temp;
            int column, row = 1, k;

            for (column = 2, k = 0; column < sizes + 2; column++, k++)
                sheet.Cells[row, column] = sizes_array[k];

            for (row = 2, k = 0; row < 2 + 2; row++, k++)
                sheet.Cells[row, 1] = methodsName[k];

            row = 2; column = 2;
            for (int i = 0; i < sizes; i++)
            {
                temp = 0;

                for (int j = i * count; j < (i + 1) * count; j++)
                    temp += method1[j] / count;
                // method1[i] = temp;
                sheet.Cells[row, column] = temp.ToString(); column++;

                //result += NameSort + "Размер: " + sizes_array[i] + " " + temp + "\n";

            }

            row = 3; column = 2;
            for (int i = 0; i < sizes; i++)
            {
                temp = 0;

                for (int j = i * count; j < (i + 1) * count; j++)
                    temp += method2[j] / count;
                // method1[i] = temp;
                sheet.Cells[row, column] = temp.ToString(); column++;

                //result += NameSort + "Размер: " + sizes_array[i] + " " + temp + "\n";

            }


            // Удалите сохраненный файл, если он уже существует.
            string filename = Application.StartupPath + "\\" + DateTime.Now.ToString().Replace(":", ".") + ".xlsx";
            //System.IO.File.Delete(filename);

            workBook.SaveAs(filename, Type.Missing, Type.Missing,
            Type.Missing, Type.Missing, Type.Missing,
            Excel.XlSaveAsAccessMode.xlExclusive, Type.Missing,
            Type.Missing, Type.Missing, Type.Missing,
            Type.Missing);

            workBook.Close(true, Type.Missing, Type.Missing);

            // Закройте сервер Excel.
            excel_app.Quit();

            return filename;

        }


        private void button2_Click(object sender, EventArgs e)
        {
            button2.Enabled = false;
            Stopwatch stopWatch = new Stopwatch();
            int[] sizes = { 10, 30, 50, 70};
            int[] array_origin = null;

            List<long> time_bubl = new List<long>();
            List<long> time_quick = new List<long>();
            
            const int count = 10;

            for (int i = 0; i < sizes.Length; i++)
            {

                array_origin = new int[sizes[i]];
                init_array(ref array_origin, sizes[i]);
                

                for (int j = 0; j < count; j++)
                {
                    CreateFile("file", array_origin);

                    stopWatch.Start();
                    NaturalFusion("file");
                    stopWatch.Stop();

                    time_bubl.Add(stopWatch.Elapsed.Ticks); stopWatch.Reset();

                    CreateFile("file", array_origin);

                    stopWatch.Start();
                    BalancedMerge("file");
                    stopWatch.Stop();

                    time_quick.Add(stopWatch.Elapsed.Ticks); stopWatch.Reset();

                }


            }

            string[] MyMethods = { "Cортировка естественным слиянием", "Сбалансированное трехпутевое слияние" };
            CreateReport(time_bubl, time_quick, count, sizes.Length, ref sizes, MyMethods);
            button2.Enabled = true;
            MessageBox.Show("Готово");
        }
    }

}
